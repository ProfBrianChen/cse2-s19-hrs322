////Generates a hand of cards, determines if hand contains pair, two pair, three of a kind or high card.
///Hrithik Sundri
///Cse 02 HW 04
//

public class PokerHandCheck{
  public static void main(String[] args) {
   
     int cardNum = (int) (Math.random() * 51) + 1;  //generates random card from deck
    
    String suit; //declares variable for suit of card
    String val; //declares variable for the value of card
    
    //card 1
    //Finds the suit of the card
    if (cardNum >= 1 && cardNum <= 13){suit = "Diamonds";}
    else if (cardNum >= 14 && cardNum <= 26){suit = "Clubs";}
    else if (cardNum >= 27 && cardNum <= 39){suit = "Hearts";}
    else {suit = "Spades";}
  
    //Finds the value of the card
    int valNumForOne = cardNum % 13;  //divides the deck into 13 cards of 1 suit
    switch (valNumForOne) {
      case 1:
        val = "Ace";
        break;      
      case 2:
        val = "2";
        break;
      case 3:
        val = "3";
        break;
      case 4:
        val = "4";
        break;
      case 5:
        val = "5";
        break;
      case 6:
        val = "6";
        break;
      case 7:
        val = "7";
        break;
      case 8:
        val = "8";
        break;
      case 9:
        val = "9";
        break;
      case 10:
        val = "10";
        break;
      case 11:
        val = "Jack";
        break;
      case 12:
        val = "Queen";
        break;
      default:
        val = "King";
        break;
    }    
  System.out.println("You picked the " + val + " of " + suit);
  
    //For card 2:
    //Generates random card
    cardNum = (int) (Math.random() * 51) + 1;
    
    //Finds the suit of the card
    if (cardNum >= 1 && cardNum <= 13){suit = "Diamonds";}
    else if (cardNum >= 14 && cardNum <= 26){suit = "Clubs";}
    else if (cardNum >= 27 && cardNum <= 39){suit = "Hearts";}
    else {suit = "Spades";}
  
    int valNumForTwo = cardNum % 13;  //Divides the deck into a universal suit of 13 cards
    
    //Finds the value of the card
    switch (valNumForTwo) {
      case 1:
        val = "Ace";
        break;      
      case 2:
        val = "2";
        break;
      case 3:
        val = "3";
        break;
      case 4:
        val = "4";
        break;
      case 5:
        val = "5";
        break;
      case 6:
        val = "6";
        break;
      case 7:
        val = "7";
        break;
      case 8:
        val = "8";
        break;
      case 9:
        val = "9";
        break;
      case 10:
        val = "10";
        break;
      case 11:
        val = "Jack";
        break;
      case 12:
        val = "Queen";
        break;
      default:
        val = "King";
        break;
    }    
  System.out.println("You picked the " + val + " of " + suit);
    
    //Card 3:
    //Generates random card
   cardNum = (int) (Math.random() * 51) + 1;
    
    //finds suit of card
    if (cardNum >= 1 && cardNum <= 13){suit = "Diamonds";}
    else if (cardNum >= 14 && cardNum <= 26){suit = "Clubs";}
    else if (cardNum >= 27 && cardNum <= 39){suit = "Hearts";}
    else {suit = "Spades";}
  
    int valNumForThree = cardNum % 13;  //divides deck into 13 cards
    
    //finds value of card
    switch (valNumForThree) {
      case 1:
        val = "Ace";
        break;      
      case 2:
        val = "2";
        break;
      case 3:
        val = "3";
        break;
      case 4:
        val = "4";
        break;
      case 5:
        val = "5";
        break;
      case 6:
        val = "6";
        break;
      case 7:
        val = "7";
        break;
      case 8:
        val = "8";
        break;
      case 9:
        val = "9";
        break;
      case 10:
        val = "10";
        break;
      case 11:
        val = "Jack";
        break;
      case 12:
        val = "Queen";
        break;
      default:
        val = "King";
        break;
    }    
  System.out.println("You picked the " + val + " of " + suit);
    
    //Card 4:
    
    cardNum = (int) (Math.random() * 51) + 1; //Generates random card
    
    //finds suit of card
    if (cardNum >= 1 && cardNum <= 13){suit = "Diamonds";}
    else if (cardNum >= 14 && cardNum <= 26){suit = "Clubs";}
    else if (cardNum >= 27 && cardNum <= 39){suit = "Hearts";}
    else {suit = "Spades";}
  
    
    int valNumForFour = cardNum % 13; //divides deck into group of 13 cards
    
    //finds value of card
    switch (valNumForFour) {
      case 1:
        val = "Ace";
        break;      
      case 2:
        val = "2";
        break;
      case 3:
        val = "3";
        break;
      case 4:
        val = "4";
        break;
      case 5:
        val = "5";
        break;
      case 6:
        val = "6";
        break;
      case 7:
        val = "7";
        break;
      case 8:
        val = "8";
        break;
      case 9:
        val = "9";
        break;
      case 10:
        val = "10";
        break;
      case 11:
        val = "Jack";
        break;
      case 12:
        val = "Queen";
        break;
      default:
        val = "King";
        break;
    }    
  System.out.println("You picked the " + val + " of " + suit);
    
    //Card 5:
    
  
    cardNum = (int) (Math.random() * 51) + 1;   //Generates random card
    
    //Finds suit of card
    if (cardNum >= 1 && cardNum <= 13){suit = "Diamonds";}
    else if (cardNum >= 14 && cardNum <= 26){suit = "Clubs";}
    else if (cardNum >= 27 && cardNum <= 39){suit = "Hearts";}
    else {suit = "Spades";}
  
    //Finds value of card
    int valNumForFive = cardNum % 13;
    switch (valNumForFive) {
      case 1:
        val = "Ace";
        break;      
      case 2:
        val = "2";
        break;
      case 3:
        val = "3";
        break;
      case 4:
        val = "4";
        break;
      case 5:
        val = "5";
        break;
      case 6:
        val = "6";
        break;
      case 7:
        val = "7";
        break;
      case 8:
        val = "8";
        break;
      case 9:
        val = "9";
        break;
      case 10:
        val = "10";
        break;
      case 11:
        val = "Jack";
        break;
      case 12:
        val = "Queen";
        break;
      default:
        val = "King";
        break;
    }    
  System.out.println("You picked the " + val + " of " + suit);
  
///////////////////
    System.out.println();
    
    //Determines whether hand contains a pair
    if (valNumForOne == valNumForTwo){
      if(valNumForOne == valNumForThree || valNumForOne  == valNumForFour || valNumForOne == valNumForFive || valNumForTwo == valNumForThree || valNumForTwo == valNumForFour || valNumForTwo == valNumForFive || valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");
      }}
    else if(valNumForOne == valNumForThree){
      if(valNumForOne  == valNumForFour || valNumForOne == valNumForFive || valNumForTwo == valNumForThree || valNumForTwo == valNumForFour || valNumForTwo == valNumForFive || valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");
      }
    }
    
    else if (valNumForOne  == valNumForFour){
      if(valNumForOne == valNumForFive || valNumForTwo == valNumForThree || valNumForTwo == valNumForFour || valNumForTwo == valNumForFive || valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }
    
    else if (valNumForOne  == valNumForFive){
      if(valNumForTwo == valNumForThree || valNumForTwo == valNumForFour || valNumForTwo == valNumForFive || valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }  
    
    else if (valNumForTwo  == valNumForThree){
      if(valNumForTwo == valNumForFour || valNumForTwo == valNumForFive || valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }
    
    else if (valNumForTwo  == valNumForFour){
      if(valNumForTwo == valNumForFive || valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }
    
    else if(valNumForTwo  == valNumForFive){
      if(valNumForThree == valNumForFour || valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }
    
    else if (valNumForThree  == valNumForFour){
      if(valNumForThree == valNumForFive || valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }
    
    else if (valNumForThree  == valNumForFive){
      if( valNumForFour == valNumForFive){
        System.out.println("You got a two pair");}
      else{
        System.out.println("You got a pair");}
      }
    
    else{
      System.out.println("You got a high card");
    }
  
  
  } //end of main method
 
}//end of class