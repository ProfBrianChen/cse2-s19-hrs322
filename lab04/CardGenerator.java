////Generates Random Cards from 52 cards
///Hrithik Sundri
///Cse 02 Lab04
//

public class CardGenerator{
  public static void main(String[] args) {
    
   int cardNum = (int) (Math.random() * 51) + 1;  //generates random card from deck
    
    String suit; //declares variable for suit of card
    String val; //declares variable for the value of card
    
    //card 1
    //Finds the suit of the card
    if (cardNum >= 1 && cardNum <= 13){suit = "Diamonds";}
    else if (cardNum >= 14 && cardNum <= 26){suit = "Clubs";}
    else if (cardNum >= 27 && cardNum <= 39){suit = "Hearts";}
    else {suit = "Spades";}
  
    //Finds the value of the card
    int valNumForOne = cardNum % 13;  //divides the deck into 13 cards of 1 suit
    switch (valNumForOne) {
      case 1:
        val = "Ace";
        break;      
      case 2:
        val = "2";
        break;
      case 3:
        val = "3";
        break;
      case 4:
        val = "4";
        break;
      case 5:
        val = "5";
        break;
      case 6:
        val = "6";
        break;
      case 7:
        val = "7";
        break;
      case 8:
        val = "8";
        break;
      case 9:
        val = "9";
        break;
      case 10:
        val = "10";
        break;
      case 11:
        val = "Jack";
        break;
      case 12:
        val = "Queen";
        break;
      default:
        val = "King";
        break;
    }    
  System.out.println("You picked the " + val + " of " + suit);
  
  
  
  
  } //end of main method
 
}//end of class