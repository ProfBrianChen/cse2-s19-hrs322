//////////This program will split the check evenly and calculate each persons tip
////Hrithik Sundri 
///Cse02 lab03

import java.util.Scanner;

public class Check {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in );
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble(); //input for subtotal
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble(); //input for amount of tip
    tipPercent /= 100; //We want to calulate the tip as a decimal
   
    System.out.print("Enter the amount of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //Number of people
    
    double totalCost; //variable for total cost
    double costPerPerson; //variable for total cost per person
    int dollars, //Whole dollar amount of cost
    dimes, //number of dimes
    pennies;//number of pennies 
    
    totalCost = checkCost * (1 + tipPercent); //Calculates total cost of check
    costPerPerson = totalCost / numPeople; //Determines how much each person pays
    
    dollars = (int)costPerPerson;   //Calculates whole dollar amount 
    
    dimes = (int)(costPerPerson * 10) % 10; //calulates number of dimes
    pennies = (int)(costPerPerson * 100) % 10;//Calculates number of pennies
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
    
  } //end of main method
}//end of class