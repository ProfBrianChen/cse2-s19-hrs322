//////////This program will print out a twist on the screen.
////Hrithik Sundri 
///Cse02 lab05

import java.util.Scanner;

public class TwistGenerator{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in );
    //variable declaration
    int UserNum;
    String junkword;
    
    System.out.print("Enter the positive number for the length: ");  //Prompts user to u=input a number
    
    //If wrong input
    while (!myScanner.hasNextInt()){
      junkword = myScanner.next();
        System.out.print("There was an error, please input another length: ");
    }
    
  UserNum = myScanner.nextInt();
    
    //beginning of the for statement for first line of twist
    
    for (int i = 0; i < UserNum; i++){
      if (i % 3 == 0){
        System.out.print("\\");
      }
      else if (i % 3 == 1){
        System.out.print(" ");
      }
      else if (i % 3 == 2){
        System.out.print("/");
      }
    }//end of for statement
    
    System.out.println();
    
    //beginning of the for statement for second line of twist
    
    
    for (int i = 0; i < UserNum; i++){
      if (i % 3 == 0){
        System.out.print(" ");
      }
      else if (i % 3 == 1){
        System.out.print("X");
      }
      else if (i % 3 == 2){
        System.out.print(" ");
      }
    } //end of for statement
    
    System.out.println();
    
    //beginning of the for statement for third line of twist
    
    
    for (int i = 0; i < UserNum; i++){
      if (i % 3 == 0){
        System.out.print("/");
      }
      else if (i % 3 == 1){
        System.out.print(" ");
      }
      else if (i % 3 == 2){
        System.out.print("\\");
      }
      
    } //end of for statement
   
    System.out.println();
    
  } //end of main method
}//end of class