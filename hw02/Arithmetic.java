///////////
////CSE 02 hw02

public class Arithmetic {
  
  public static void main(String args[]) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost of pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //Cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants; //Total cost of pants
    double totalCostOfSweatshirts; //Total cost of pants
    double totalCostOfBelts; //Total cost of belts
    
    double salesTaxPants; //Sales Tax charged on pants
    double salesTaxSweatshirts; //Sales Tax charged on sweatshirts
    double salesTaxBelts; //Sales Tax charged on belts
    
    double subtotal; //The total Cost of the purchase before tax
    
    double totalSalesTax; //The Total Sales Tax
    
    double totalPaid;  //Total paid for entire purchase with sales tax
    
    ///Assigning Variables
    
    totalCostOfPants = numPants * pantsPrice; //Calculates Total Cost of pants
    totalCostOfSweatshirts = numShirts * shirtPrice; //Calculates Total Cost of sweatshirts
    totalCostOfBelts = numBelts * beltCost;  //Calculates Total Cost of belts
    
    salesTaxPants = totalCostOfPants * paSalesTax; //Calculates Sales Tax on pants
    salesTaxSweatshirts = totalCostOfSweatshirts * paSalesTax; //Calculates Sales Tax on sweatshirts
    salesTaxBelts = totalCostOfBelts * paSalesTax; //Calculates Sales Tax on belts
    
    subtotal = totalCostOfPants + totalCostOfSweatshirts + totalCostOfBelts; //Calculates Total cost before tax
    totalSalesTax = salesTaxPants + salesTaxSweatshirts + salesTaxBelts; //Calculates Total sales tax 
    totalPaid = subtotal + totalSalesTax; //Calculates Total after tax
    
    System.out.println ("The subtotal for pants is $" + totalCostOfPants);
    System.out.println ("The total sales tax for pants are $" + ((int)(salesTaxPants * 100) /100.00));
    
    System.out.println ("The subtotal for sweatshirts is $" + totalCostOfSweatshirts);
    System.out.println ("The total sales tax for sweatshirts are $" + (int)(salesTaxSweatshirts * 100) /100.00);
    
    System.out.println ("The subtotal for belts is $" + totalCostOfBelts);
    System.out.println ("The total sales tax for belts are $" + (int)(salesTaxBelts * 100)/100.00);
    
    System.out.println ("The subtotal of everything is $" + subtotal);
    System.out.println ("The Total Sales Tax for everything is $" + (int)(totalSalesTax * 100)/100.00);
    System.out.println("The Total paid for everything is $" + (int)(totalPaid * 100)/100.00);
    
    
  }//end of main method
}//end of class