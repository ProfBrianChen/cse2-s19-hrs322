///////////Hrithik Sundri
////CSE 02 hw06


import java.util.Scanner;
public class Network { 
  public static void main(String args[]) {
    Scanner myScanner = new Scanner ( System.in );
    
    int height = 0; //variable for height
    int width = 0;  //variable for width
    int edge = 0; //variable for edge
    int size  = 0; //variable for size of box
    
    do{ //Checks for height
      System.out.print("Input desired height: ");
      while(!myScanner.hasNextInt()){
        System.out.print("Please enter a positive integer: ");
        myScanner.next();
      }
      height = myScanner.nextInt();
      } while(height <= 0);
    
    do{  //Checks for width
      System.out.print("Input desired width: ");
      while(!myScanner.hasNextInt()){
        System.out.print("Please enter a positive integer: ");
        myScanner.next();
      }
      width = myScanner.nextInt();
      } while(width <= 0);
    
    do{ //Checks for length  of edge
      System.out.print("Input desired edge: ");
      while(!myScanner.hasNextInt()){
        System.out.print("Please enter a positive integer: ");
        myScanner.next();
      }
      edge = myScanner.nextInt();
      } while(edge <= 0);
    
    do{ //Checks for size
      System.out.print("Input desired size: ");
      while(!myScanner.hasNextInt()){
        System.out.print("Please enter a positive integer: ");
        myScanner.next();
      }
      size = myScanner.nextInt();
      } while(size <= 0);
    
//////    //////    //////    //////    //////    //////    //////    //////    ////////////    //////    //////    //////    //////    //////    //////    //////    //////
    
    for(int x = 1; x <= width; x ++){
      for (int y = 1; y <= height; y ++){
        if (x == 1 || x == size){
          if ( y == size || y == 1){
            System.out.print("#");
          }
          else{
            System.out.print("-");
          }
        }
        System.out.println();
      }

    }
    
    
    
    } //end of method
} //end of class
    