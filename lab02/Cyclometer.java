//////This program will measure the speed, distance, etc, record two kinds of data, the time elapsed in seconds and the number of rotations the front wheel during that time
///Hrithik Sundri
///Cse 02 Lab02
//
public class Cyclometer {
  public static void main(String[] args) {
    
    
    ///input data
    int secsTrip1=480; //Counts how many seconds are in trip 1
    int secsTrip2=3220; //Counts how many seconds are in trip 2
    int countsTrip1=1561; //number of rotations in trip 1
    int countsTrip2=9037; //number of rotations in trip 2
    
    double wheelDiameter = 27.0;
    double PI = 3.14159;
    double feetPerMile = 5280;
    double inchesPerFoot = 12;
    double secondsPerMinute = 60;
    
    
    double distanceTrip1, distanceTrip2, totalDistance;
    
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had " + countsTrip1+" counts.");
    
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had " + countsTrip2+" counts.");
    
    distanceTrip1 = countsTrip1  * wheelDiameter * PI; //Distance in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; //Distance in miles
    distanceTrip2=countsTrip2 * wheelDiameter * PI/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
     System.out.println("Trip 1 was "+distanceTrip1+" miles");
	   System.out.println("Trip 2 was "+distanceTrip2+" miles");
	   System.out.println("The total distance was "+totalDistance+" miles");

    
    
    
  } //end of main method
 
}//end of class