///////////
//// Hrithik Sundri Cse 02 hw03
//This program will convert meters to inches

import java.util.Scanner;

public class Convert{
  public static void main(String args[]) {
    Scanner myScanner = new Scanner ( System.in );  
    System.out.println("Enter the distance in meters ");
    double totalMeters = myScanner.nextDouble();
    
    double totalInches = totalMeters * 39.37 ;
    System.out.println ( totalMeters + " meters is " + (int)(totalInches * 100) / 100.0000 + " inches.");
  
    
}//end of main method

}//end of class