///////////
////Hrithik Sundri Cse 02 hw03
///This program will calculate the volume of ithe inside of a box.

import java.util.Scanner;

public class BoxVolume{
  public static void main(String args[]) {
    Scanner myScanner = new Scanner ( System.in );
    
    System.out.print("The width side of the box is: ");
    int totalWidth = myScanner.nextInt();
    System.out.print("The length of the box is: ");
    int totalLength = myScanner.nextInt();
    System.out.print("The height of the box is: ");
    int totalHeight= myScanner.nextInt();
    
    int totalVolume = totalWidth * totalLength * totalHeight;
    
    System.out.println("The volume of the box is " + totalVolume);
  
  }//end of main method
}//end of class